# Implementation:

### Q) What libraries did you add to the frontend? What are they used for?
The only library added is [date-fns](https://date-fns.org/) to handle date format and operations on date.
I also added some flow integration to handle basic type checking and prettier for the file formatting.
### Q) What is the command to start the server?

(Default) `APIKEY=<key> npm run server`

---

# General:

### Q) How long, in hours, did you spend on the test?
Around 10, probably a little more, let's say 2 / 3 hour per day.

### Q) If you had more time, what further improvements or new features would you add?
A better error handling in the back end.  
I wanted also to add tests but I had problem to run jest with the current set up and I had to give up at one point.

### Q) Which parts are you most proud of? And why?
I like the `data-utils` section in the server side. It does all the job, a good data cleanup to make the front end life easier.

### Q) Which parts did you spend the most time with? What did you find most difficult?
I didn't find anything particularly difficult in the test. I found hard to understand the APIs at the beginning but mostly because it was usually night time and I probably couldn't concentrate as needed.  
I probably spent more time doing the UI finging the right component or the right scss variable.

### Q) How did you find the test overall? If you have any suggestions on how we can improve the test or our API, we'd love to hear them.
I think it was a good test. I found it reasonable in terms of challanges and time expected. I would add tests configuration and add it as a plus in the test.  
Talking about the UI I felt like dates are missing. At one point I asked myself if flights' date were correct or not and I had to look at the network tab or in the terminal to have this information.
