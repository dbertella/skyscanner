const pino = require('pino')();

// obtain API key from your Skyscanner contact
const APIKEY = process.env.APIKEY;

if (!APIKEY) {
  pino.error(
    'APIKEY environment variable missing. ' +
      'Please re-run with `APIKEY=<key> npm run server`'
  );
  process.exit(1);
}

module.exports = {
  apiKey: APIKEY,
  skyscannerApi: 'http://partners.api.skyscanner.net/'
};
