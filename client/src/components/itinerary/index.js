// @flow
import React from 'react';
import BpkCard from 'bpk-component-card';
import BpkButton from 'bpk-component-button';

import Flight from '../flight';
import type { Itinerary as ItineraryType } from '../../types';
import './style.scss';

type Props = {
  itinerary: ItineraryType
};

const Itinereary = ({ itinerary }: Props) =>
  <BpkCard className="itinerary">
    <Flight data={itinerary.outbound} />
    <Flight data={itinerary.inbound} />
    <div className="itinerary-info">
      <div>
        <div className="itinerary-price color-dark-gray">
          £{itinerary.price}
        </div>
        <div className="itinerary-agent">
          {itinerary.agent.name}
        </div>
      </div>
      <BpkButton href={itinerary.deeplink}>
        <span className="itinerary-select-button">Select</span>
      </BpkButton>
    </div>
  </BpkCard>;

export default Itinereary;
