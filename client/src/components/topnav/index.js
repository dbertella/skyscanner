// @flow
import React from 'react';
import BpkMenuIcon from 'bpk-component-icon/lg/menu';
import logo from '../../logo.svg';

import './style.scss';

const TopNav = () =>
  <header className="topNav">
    <a href="/">
      <span className="logoText">Skyscanner</span>
      <img className="logo" alt="Skyscanner" src={logo} />
    </a>
    <BpkMenuIcon className="menu" />
  </header>;

export default TopNav;
