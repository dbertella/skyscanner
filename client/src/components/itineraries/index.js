// @flow
import React from 'react';
import { BpkLargeSpinner } from 'bpk-component-spinner';

import Itinerary from '../itinerary';
import type { Itineraries as ItinerariesType } from '../../types';

import './style.scss';

type Props = {
  itineraries: ItinerariesType,
  isFetching: boolean,
  error: ?string
};

const Itineraries = ({ itineraries, isFetching, error }: Props) => {
  if (error) {
    return (
      <div className="placeholder">
        {error}
      </div>
    );
  } else if (isFetching) {
    return <BpkLargeSpinner className="placeholder" />;
  }
  return (
    <div className="itineraries">
      {itineraries.map(itinerary =>
        <Itinerary key={itinerary.id} itinerary={itinerary} />
      )}
    </div>
  );
};

export default Itineraries;
