// @flow
import React from 'react';
import BpkArrowRightIcon from 'bpk-component-icon/lg/long-arrow-right';

import { pluralize } from '../../utils';
import './style.scss';

type Params = {
  adults: string,
  class: string,
  originplace: string,
  destinationplace: string,
  outbounddate: string,
  inbounddate: string
};

const Header = ({ params }: { params: Params }) =>
  <div className="header">
    <div className="airports">
      {params.originplace}
      <BpkArrowRightIcon className="arrow" />
      {params.destinationplace}
    </div>
    <div>
      {params.adults} {pluralize('traveller', parseInt(params.adults, 10))},{' '}
      {params.class.toLowerCase()}
    </div>
  </div>;

export default Header;
