require('isomorphic-fetch');
require('es6-promise').polyfill();

const express = require('express');
const app = express();
const api = require('./api/');
const dataUtils = require('./data-utils');
const pino = require('pino')();

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  next();
});

app.get('/', (req, res) => {
  res.send('Hello World!');
});

/**
  Simple flight search api wrapper.

  TODO: client should provide params

  Api params and location values are here:
  http://business.skyscanner.net/portal/en-GB/Documentation/FlightsLivePricingQuickStart
*/

app.get('/api/search', (req, res) => {
  api.livePricing
    .search(req.query)
    .then(results => {
      // A better format for displaying results to the client
      pino.info('Transform results for consumption by client');
      const itineraries = dataUtils(results);

      res.json({
        itineraries,
        fetchingComplete: results.Status === 'UpdatesComplete'
      });
    })
    .catch(error => {
      pino.error(error);
      res.status(500).send({ error: 'Something failed!' });
    });
});

app.listen(4000, () => {
  pino.info('Node server listening on http://localhost:4000');
});
