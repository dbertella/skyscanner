// @flow
import React from 'react';
import format from 'date-fns/format';
import BpkArrowRightIcon from 'bpk-component-icon/sm/long-arrow-right';

import { pluralize } from '../../utils';
import type { Flight as FlightType } from '../../types';
import './style.scss';

type Props = {
  data: FlightType
};

const Flight = ({ data }: Props) =>
  <div className="flight">
    <div className="flight-info">
      <img
        className="flight-img"
        src={data.carrier.image}
        alt={data.carrier.name}
      />
      <div className="flight-single">
        <div className="color-dark-gray line-height-1">
          {format(data.departureTime, 'HH:mm')}
        </div>
        <div className="flight-airport-code">
          {data.originStation.code}
        </div>
      </div>
      <BpkArrowRightIcon className="flight-icon" />
      <div className="flight-single">
        <div className="color-dark-gray line-height-1">
          {format(data.arrivalTime, 'HH:mm')}
        </div>
        <div className="flight-airport-code">
          {data.destinationStation.code}
        </div>
      </div>
    </div>
    <div className="flight-duration">
      <div className="line-height-1">
        {data.duration}
      </div>
      <div className="flight-direct">
        {!!data.stopsNumber
          ? `${data.stopsNumber} ${pluralize('Stop', data.stopsNumber)}`
          : 'Direct'}
      </div>
    </div>
  </div>;

export default Flight;
