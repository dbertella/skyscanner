const _ = require('lodash');

const getDurationFormat = duration => {
  const h = Math.floor(duration / 60);
  const m = duration % 60;
  const H = h ? `${h}h` : '';
  const M = m ? ` ${m}` : '';
  return H + M;
};

const dataUtils = results => {
  const places = results.Places.map(place => ({
    id: place.Id,
    code: place.Code
  }));

  const carriers = results.Carriers.map(carrier => ({
    id: carrier.Id,
    name: carrier.Name,
    image: `https://logos.skyscnr.com/images/airlines/favicon/${carrier.DisplayCode}.png`
  }));

  const legs = results.Legs.map(leg => ({
    id: leg.Id,
    carrier: carriers.find(carrier => carrier.id === leg.Carriers[0]),
    originStation: places.find(place => place.id === leg.OriginStation),
    destinationStation: places.find(
      place => place.id === leg.DestinationStation
    ),
    departureTime: leg.Departure,
    arrivalTime: leg.Arrival,
    duration: getDurationFormat(leg.Duration),
    stopsNumber: leg.Stops.length
  }));

  const agents = results.Agents.map(agent => ({
    id: agent.Id,
    name: agent.Name
  }));

  return results.Itineraries.map((itinerary, i) => {
    const pricingOption = _.chain(itinerary.PricingOptions)
      .sortBy('Price')
      .take(1)
      .value()[0];
    return {
      id: i,
      outbound: legs.find(leg => leg.id === itinerary.OutboundLegId),
      inbound: legs.find(leg => leg.id === itinerary.InboundLegId),
      price: pricingOption.Price,
      deeplink: pricingOption.DeeplinkUrl,
      agent: agents.find(agent => agent.id === pricingOption.Agents[0])
    };
  });
};

module.exports = dataUtils;
