// @flow
import React from 'react';
import BpkPriceAlertIcon from 'bpk-component-icon/lg/price-alerts';

import './style.scss';

const Controls = () =>
  <div className="controls">
    <div>
      <span className="controls-button">Filter</span>
      <span className="controls-button">Sort</span>
    </div>
    <div className="controls-price">
      <BpkPriceAlertIcon className="controls-icon" />
      <span>Price alerts</span>
    </div>
  </div>;

export default Controls;
