// @flow
import React, { Component } from 'react';

import TopNav from './components/topnav';
import Header from './components/header';
import Controls from './components/controls';
import Itineraries from './components/itineraries';
import {
  fetchApi,
  getSelectedDayInAWeek,
  onFetchSuccess,
  onFetchError
} from './utils';

import type { Itineraries as ItinerariesType, Params } from './types';
import './App.scss';

type State = {
  itineraries: ItinerariesType,
  fetchingComplete: boolean,
  params: Params,
  error: ?string
};

class App extends Component {
  state: State = {
    itineraries: [],
    fetchingComplete: false,
    params: {
      adults: '2',
      class: 'Economy',
      originplace: 'EDI',
      destinationplace: 'LHR',
      outbounddate: getSelectedDayInAWeek(1),
      inbounddate: getSelectedDayInAWeek(2)
    },
    error: null
  };
  componentDidMount() {
    const { params } = this.state;
    fetchApi(params)
      .then(results => {
        this.setState(onFetchSuccess(results));
      })
      .catch(() => this.setState(onFetchError));
  }
  render() {
    const { itineraries, fetchingComplete, params, error } = this.state;
    return (
      <div className="App">
        <TopNav />
        <Header params={params} />
        <Controls />
        <Itineraries
          itineraries={itineraries}
          isFetching={!fetchingComplete}
          error={error}
        />
      </div>
    );
  }
}

export default App;
