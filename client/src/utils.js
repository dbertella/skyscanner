// @flow
import addWeeks from 'date-fns/add_weeks';
import format from 'date-fns/format';
import isSunday from 'date-fns/is_sunday';
import setDay from 'date-fns/set_day';

import type { Itineraries, Params } from './types';

const API_URL = 'http://localhost:4000/api/';
const DATE_FORMAT = 'YYYY-MM-DD';

const queryParams = (params: Params) =>
  Object.keys(params)
    .map(k => `${encodeURIComponent(k)}=${encodeURIComponent(params[k])}`)
    .join('&');

const parseJSON = response => response.json();

const checkStatus = response => {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }

  throw new Error(response.statusText);
};

export const fetchApi = (params: Params) => {
  const url = `${API_URL}search?${queryParams(params)}`;

  console.log('fetching results from server...');
  return fetch(url).then(checkStatus).then(parseJSON).then(result => result);
};

export const onFetchError = () => ({
  fetchingComplete: true,
  error: 'Something failed!'
});

type Results = {
  itineraries: Itineraries,
  fetchingComplete: boolean
};
export const onFetchSuccess = (results: Results) => () => ({
  itineraries: results.itineraries,
  fetchingComplete: results.fetchingComplete,
  error: null
});

export const getSelectedDayInAWeek = (dayNum: number) => {
  const date = new Date();
  // Sunday is the first day of the week.
  // Make sure not to add a week if it's sunday.
  const dateNextWeek = isSunday(date) ? date : addWeeks(date, 1);
  return format(setDay(dateNextWeek, dayNum), DATE_FORMAT);
};

export const pluralize = (
  word: string,
  quantity: number,
  plural?: string = 's'
) => (quantity > 1 ? word + plural : word);
